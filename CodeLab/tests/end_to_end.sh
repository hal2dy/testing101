#!/bin/bash
#should run at root

dir=$(pwd)
url_cart=http://localhost:9726/your_cart.php
url_request=http://localhost:9726/server_request_data.php?userid=1234
expect='[{"item":"Plush lobster","quantity":"1"},{"item":"Plush lobster food","quantity":"10"}]'

server_root=$dir/../apache_temp
server_config_file=$dir/../testing101.conf
pid_file=$dir/../apache_temp/run/httpd.pid

#start server
httpd -d $server_root -f $server_config_file 2>&1

#check server
data=$(curl -sb -H $url_cart)
if [[ $data != *"<html>"* ]]
then
    $(kill $(cat $pid_file))
    exit 1
fi

#check data 
#not work
data=$(curl $url_request)
if [[ $data != $expect ]]
then
    $(kill $(cat $pid_file))
    exit 2
fi

#stop server
$(kill $(cat $pid_file))
exit 0
