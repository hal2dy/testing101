<?php
require_once "phplib/ShowCartController.php";

/**
 * Unit tests for ShowCartControllerTest.
 */
class ShowCartControllerTest extends PHPUnit_Framework_TestCase {

    public function testRequestData() {
        $this->expectOutputString('<html>
<head><title>Your Cart</title></head>
<body>
<table>
    <tr><th>Item name</th><th>Quantity</th></tr>
    <tr>
        <td>Plush lobster</td>
        <td>1</td>
    </tr>
    <tr>
        <td>Plush lobster food</td>
        <td>10</td>
    </tr>
</table>
</html>');

        $expectResult  = [["item" => "Plush lobster", "quantity" => "1"], ["item" => "Plush lobster food", "quantity" => "10"]];
        $ctr = $this->getMockBuilder('ShowCartController')
            ->setMethods(array('getCardData'))
            ->getMock();

        $ctr->expects($this->once())
            ->method('getCardData')
            ->will($this->returnValue($expectResult));

        $ctr->showCart();
    }
    
    public function testRequestDataReturnNull() {
        $this->expectOutputString('<html>
<head><title>Your Cart</title></head>
<body>
<table>
    <tr><th>Item name</th><th>Quantity</th></tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
</table>
</html>');

        $ctr = $this->getMockBuilder('ShowCartController')
            ->setMethods(array('getCardData'))
            ->getMock();

        $ctr->showCart();
    }

    public function testRequestDataReturnBlank() {
        $this->expectOutputString('<html>
<head><title>Your Cart</title></head>
<body>
<table>
    <tr><th>Item name</th><th>Quantity</th></tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
    </tr>
</table>
</html>');

        $ctr = $this->getMockBuilder('ShowCartController')
            ->setMethods(array('getCardData'))
            ->getMock();

        $ctr->expects($this->once())
            ->method('getCardData')
            ->will($this->returnValue(''));

        $ctr->showCart();
    }
}
