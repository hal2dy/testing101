<?php
require_once "phplib/ApiClient.php";

class HttpRequestTest extends HttpRequest {
    public function __construct($num = 0) {
        $this->retry = $num;
    }

    public function do_request($url) {
        $data = [["item" => "Plush lobster", "quantity" => "1"]];
        while ($this->retry > 0) {
            $this->retry--;
            return ['', array('http_code' => 404)];
        }
        return [json_encode($data, true), array('http_code' => 200)];
    }
}

class HttpRequestTestNull extends HttpRequest {
    public function do_request($url) {
        return null;
    }
}

/**
 * Unit tests for ApiClient.
 */
class ApiClientTest extends PHPUnit_Framework_TestCase {

    public function testRequestData() {
        $expectResult = [["item" => "Plush lobster", "quantity" => "1"]];
        $req = new HttpRequestTest();
        $client = new ApiClient($req);
        $result = $client->getData();
        $this->assertEquals($expectResult, $result);
        $this->assertEquals(0, $client->getRetryTime());
    }

    public function testRequestDataRetry() {
        $expectResult = [["item" => "Plush lobster", "quantity" => "1"]];
        $trytime = 1;
        $req = new HttpRequestTest($trytime);
        $client = new ApiClient($req);
        $client->setRetryBackoffIntervals([0, -1]);
        $result = $client->getData();
        $this->assertEquals($expectResult, $result);
        $this->assertEquals($trytime, $client->getRetryTime());
    }

    public function testRequestDataRetryExpire() {
        $this->setExpectedException('RuntimeException');
        $trytime = 1;
        $req = new HttpRequestTest($trytime);
        $client = new ApiClient($req);
        $client->setRetryBackoffIntervals([]);
        $result = $client->getData();
    }

    public function testRequestDataReturnNull() {
        $this->setExpectedException('RuntimeException');
        $req = new HttpRequestTestNull();
        $client = new ApiClient($req);
        $client->setRetryBackoffIntervals([]);
        $result = $client->getData();
    }
}
