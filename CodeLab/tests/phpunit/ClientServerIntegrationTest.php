<?php
require_once "phplib/Testing/BackgroundAppRunner.php";
require_once "phplib/ApiClient.php";

class HttpRequestTestCustomUrl extends HttpRequest {

    private $url;

    public function setUrl($url) {
        $this->url = $url;
    }

    public function do_request($url) {
        $url = $this->url;
        return parent::do_request($url);
    }
}

/**
 * Unit tests for ClientServerIntegrationTest.
 */
class ClientServerIntegrationTest extends PHPUnit_Framework_TestCase {

    private $appRunner = null;

    protected function setUp() {
        $this->appRunner = new Testing_BackgroundAppRunner();
        $this->appRunner->start();
    }

    protected function tearDown() {
        if (!is_null($this->appRunner)) {
            $this->appRunner->stop();
            $this->appRunner = null;
        }
    }

    public function testRequestData() {
        $expectResult = [["item" => "Plush lobster", "quantity" => "1"], ["item" => "Plush lobster food", "quantity" => "10"]];
        $req = new HttpRequest();
        $client = new ApiClient($req);
        $result = $client->getData();
        $this->assertEquals($expectResult, $result);
        $this->assertEquals(0, $client->getRetryTime());
    }

    public function testRequestDataWrongParams() {
        $url = "http://localhost:9726/server_request_data.php?userid=x";
        $req = new HttpRequestTestCustomUrl();
        $req->setUrl($url);
        $client = new ApiClient($req);
        $client->setRetryBackoffIntervals([1, -1]);
        $result = $client->getData();
        $this->assertEquals(null, $result);
        $this->assertEquals(0, $client->getRetryTime());
    }

    public function testRequestDataMissingParams() {
        $url = "http://localhost:9726/server_request_data.php";
        $req = new HttpRequestTestCustomUrl();
        $req->setUrl($url);
        $client = new ApiClient($req);
        $client->setRetryBackoffIntervals([1, -1]);
        $result = $client->getData();
        $this->assertEquals(null, $result);
        $this->assertEquals(0, $client->getRetryTime());
    }

    public function testRequestDataWrongUrl() {
        $this->setExpectedException('RuntimeException');
        $url = "http://localhost.dummy.notfound";
        $req = new HttpRequestTestCustomUrl();
        $req->setUrl($url);
        $client = new ApiClient($req);
        $client->setRetryBackoffIntervals([1, -1]);
        $result = $client->getData();
        $this->assertEquals(1, $client->getRetryTime());
    }

    public function testRequestDataServerNotStart() {
        //stop server
        $this->appRunner->stop();
        $this->appRunner = null;

        $this->setExpectedException('RuntimeException');
        $req = new HttpRequest();
        $client = new ApiClient($req);
        $client->setRetryBackoffIntervals([1, -1]);
        $result = $client->getData();
        $this->assertEquals(1, $client->getRetryTime());
    }
}
