<?php

require_once 'ApiClient.php';

class ShowCartController {

    /**
     * Renders the contents of a user's cart.
     */
    public function getCardData() {
        $req = new HttpRequest();
        $client = new ApiClient($req);
        $cartData = $client->getData();
        return $cartData;
    }

    public function buildHtml($cartData) {
        echo <<<HTML
<html>
<head><title>Your Cart</title></head>
<body>
<table>
    <tr><th>Item name</th><th>Quantity</th></tr>
    <tr>
        <td>{$cartData[0]["item"]}</td>
        <td>{$cartData[0]["quantity"]}</td>
    </tr>
    <tr>
        <td>{$cartData[1]["item"]}</td>
        <td>{$cartData[1]["quantity"]}</td>
    </tr>
</table>
</html>
HTML;
    }

    public function showCart() {
        $cartData = $this->getCardData();
        if ( ! $cartData)
            $cartData = [["item" => "", "quantity" => ""], ["item" => "", "quantity" => ""]];
        $this->buildHtml($cartData);
    }
}
