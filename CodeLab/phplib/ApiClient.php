<?php

/**
 * Fetches API data.
 */
class ApiClient {

    const API_SERVER_URL = "localhost:9726";
    const FETCH_DATA_ENDPOINT = "/server_request_data.php";

    private $retryBackoffIntervals = [1, 10, 30, -1];
    private $retryTime = 0;

    public function __construct($http_quest) {
        $this->http_request = $http_quest;
    }

    /**
     * Attempts to retrieve and parse API data from some source, backing off and retrying a total of four times.
     *
     * @return array API data
     * @throws RuntimeException if there's any error retrieving the data
     */
    public function getData() {
        foreach($this->retryBackoffIntervals as $oneRetryBackoffInterval) {
            $url = "http://" . self::API_SERVER_URL . self::FETCH_DATA_ENDPOINT . "?userid=1234";
            list($dataString, $resultInfo) = $this->http_request->do_request($url);
            $responseCode = $resultInfo["http_code"];
            if ($responseCode == 200) {
                return json_decode($dataString, true);
            } elseif ($oneRetryBackoffInterval >= 0) {
                error_log("Curl failed. Sleeping for $oneRetryBackoffInterval seconds...\n", 3, "php://stderr");
                sleep($oneRetryBackoffInterval);
                $this->retryTime++;
            }
        }

        throw new RuntimeException("Failed getting data!");
    }

    public function setRetryBackoffIntervals($data) {
        $this->retryBackoffIntervals = $data;
    }

    public function getRetryTime() {
        return $this->retryTime;
    }

}

class HttpRequest {
    public function do_request($url) {
        $curl_handle = curl_init();
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_URL, $url);

        $dataString = curl_exec($curl_handle);
        $resultInfo = curl_getinfo($curl_handle);
        return [$dataString, $resultInfo];
    }
}
